package com.gb.multiplicationtableapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiplicationTableApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiplicationTableApiApplication.class, args);
    }

}
